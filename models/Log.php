<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property string|null $descricao
 * @property string|null $data_hora_busca
 * @property string|null $principais_pratos
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data_hora_busca'], 'safe'],
            [['principais_pratos'], 'string'],
            [['descricao'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descricao',
            'data_hora_busca' => 'Data Hora Busca',
            'principais_pratos' => 'Principais Pratos',
        ];
    }
}
