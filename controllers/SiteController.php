<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Log;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionObterPratosPorCidade($cidade)
    {
        
        $erro = "";

        $data_hora_inicial  = date("Y-m-d")." 00:00:00";
        $data_hora_atual    = date("Y-m-d H:i:s");

        $logs = Log::find() ->andWhere([">=", "data_hora_busca", $data_hora_inicial])
                            ->andWhere(["<=", "data_hora_busca", $data_hora_atual])
                            ->count();

        if($logs >= 20){
            $erro = "Limite diário de pesquisas atingido.";
            $pratos_retorno["erro"] = "Limite diário de pesquisas atingido.";
            return json_encode($pratos_retorno, JSON_UNESCAPED_UNICODE);
        }

        $temperatura_return             = $this->obterTemperatura($cidade);
        $temperatura                    = $temperatura_return["temperatura"];
        $cidade_nome                    = $temperatura_return["cidade"];
        $pratos_retorno["cidade"]       = $cidade_nome;
        $pratos_retorno["temperatura"]  = $temperatura;
        
        //echo "Temperatura da cidade de ".$cidade_nome.": ".number_format($temperatura, 2, ',', '.')."";

        if($temperatura >= 30){
            $pratos_retorno["pratos"]["Saladas"] = $this->obterPratos("salada");
        }
        elseif($temperatura >= 20 && $temperatura < 30){
            $pratos_retorno["pratos"]["Cereais"] = $this->obterPratos("cereal");
            $pratos_retorno["pratos"]["Carnes"] = $this->obterPratos("carne"); 
        }
        else{
            $pratos_retorno["pratos"]["Sopa"] = $this->obterPratos("sopa");
            $pratos_retorno["pratos"]["Caldo"] = $this->obterPratos("caldo");
        }

        return json_encode($pratos_retorno, JSON_UNESCAPED_UNICODE);

        //Se a temperatura estiver acima de 30º C sugira uma salada;
        //Se a temperatura estiver entre 20º C e 29º C sugira uma prato com cereais e/ou carnes;
        //Se a temperatura estiver abaixo de 20º C sugira sopas ou/e caldos.
    }

    public function obterTemperatura($cidade){

        //OPENWEATHER
        $app_key    = "695bf1f69c13f17c84453382c06654b1";

        $url        = "http://api.openweathermap.org/data/2.5/weather?q=".$cidade."&appid=".$app_key;
        $ch         = curl_init( $url );

        $body       = null;

        curl_setopt($ch, CURLOPT_POSTFIELDS, $body );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        $info   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $return["body"]   = json_decode($result, JSON_UNESCAPED_UNICODE);
        $return["httpCode"] = $info;
        $return["urlTeste"] = $url;
        $return["filter"]     = json_decode($body);
        curl_close($ch);

        if($info != 200){
            echo "Cidade não encontrada";
            return null;
        }
        else{
            $temperatura_kelvin     = $return["body"]["main"]["temp"];
            $temperatura_celsius    = $temperatura_kelvin - 273.15;
            
            return ["cidade"=>$return["body"]["name"], "temperatura"=>$temperatura_celsius];
        }

        echo "<pre>";
        print_r($return);
        echo "</pre>";



    }

    public function obterPratos($tipos_pratos){

        //EDAMAM
        $app_id = "85a255d2";
        $app_key    = "dc44ef0cca044716b9fd20ea8a34ce6f";

        $url        = "https://api.edamam.com/api/food-database/v2/parser?app_id=".$app_id."&app_key=".$app_key."&ingr=".$tipos_pratos;
        $ch         = curl_init( $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        $info   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $return["body"]   = json_decode($result, JSON_UNESCAPED_UNICODE);
        $return["httpCode"] = $info;
        $return["urlTeste"] = $url;
        curl_close($ch);

        $pratos = [];
        foreach($return["body"]["hints"] as $dicas){
            $pratos[] = $dicas["food"]["label"]; 
        }

        return $pratos;

        echo "<pre>";
        print_r($return["body"]["hints"]);
        echo "</pre>";

    }

}
